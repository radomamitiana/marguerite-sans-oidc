# Project 

Based on template for java-node project. This project is generated by gke-stack-provision pipeline.

##  URLs
*Available once the environment is build and deployment is done*

### Front-end

- DEV: https://dsb-app.dev1.applis.renault.fr
- RE7: https://dsb-app.re7.applis.renault.fr
- OPE: https://dsb-app.ope.applis.renault.fr


### Back-end

- DEV: https://dsb-api.dev1.applis.renault.fr
- RE7: https://dsb-api.re7.applis.renault.fr
- OPE: https://dsb-api.ope.applis.renault.fr


### Environment variables

All environment variables are configured on the Settings of your project.
You need to update only the `CI_REGISTRY_PASSWORD` and the `CI_REGISTRY_USER` variables for DTR Access.

### Database information _(Optionnal)_

Update in the values configuration file for each environment:
 - The instance name of your database (to get it from cloud transformation team)
 - The environments values (`DB_NAME`, `DB_HOST`, `DB_PORT`)
 - The `DB_USER` and `DB_PASSWORD` are obtained from the kubernets secret `cloudsql-db-credentials` stored in the namespace of your project 


### GCS information _(Optionnal)_

If you have created a gcs for your project:
 - The gcs name is : dsb-70295-env which env is dev, int and ope.
 - A service account is created to acces to the bucket
 - The private key of the service account to access is stored in the kubernets secret `gcs-bucket-credentials` in the namesapce dsb-70295 of your project


## Useful links to start your project

- [Link](https://confluence.dt.renault.com/display/J2C/GCP+Migration%3A+All+steps) to Cloud Transformation all steps migration
- [Link](https://confluence.dt.renault.com/pages/viewpage.action?pageId=27035384) to format correctly your logs. It is important to correctly format logs with Renault standard in order to debug and fullfill this requirement
- [Link](https://gitlabee.dt.renault.com/cloud-migration/common/gke-stack) our project reference