package com.renault.dsb.controller.MonitoringController;

import com.renault.dsb.data.dto.MonitoringsDTO;
import com.renault.dsb.repository.marguerite.MonitoringRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import com.renault.dsb.data.dto.ResponseDTO;
import com.renault.dsb.service.MonitoringService.MonitoringService;

@RestController
@RequestMapping(value = "/api/MitoringController")
public class MonitoringController {

    @Autowired
    private MonitoringService monitoringService;

    @Autowired
    private MonitoringRepository monitoringRepository;

    @GetMapping(path = "/insertInto", params = {"type"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseDTO insertInto(@RequestParam(value = "type") String type, @RequestParam(value = "ipn") String ipn) {
        return monitoringService.insertInto();
    }

    @DeleteMapping(path = "/deleteAll", params = {"type"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public boolean deleteAll(@RequestParam(value = "type") String type, @RequestParam(value = "ipn") String ipn) {
        monitoringRepository.deleteAll();
        return true;
    }

    @GetMapping(value = {"/list"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public MonitoringsDTO listMonitoring(@RequestParam(value = "page", defaultValue = "0", required = false) int page,
                                      @RequestParam(value = "size", defaultValue = "20", required = false) int size) {
        return monitoringService.listMonitoring(PageRequest.of(page, size));
    }

}
