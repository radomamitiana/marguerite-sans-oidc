package com.renault.dsb.controller.emballage;

import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.EmballageDTO;
import com.renault.dsb.data.dto.EmballagesDTO;
import com.renault.dsb.service.emballage.EmballageService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/emballage")
public class EmballageController {

    @Autowired
    private EmballageService emballageService;

    /**
     * @param emballageDTO
     * @return
     */
    @PostMapping(value = {"/createOrUpdate"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public EmballageDTO createOrUpdate(@Valid @RequestBody EmballageDTO emballageDTO) {

        return emballageService.createOrUpdate(emballageDTO);
    }

    /**
     * @param id
     * @return
     */
    @DeleteMapping(path = "/delete", params = {"id"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public IsDeletedDTO delete(@RequestParam(value = "id") Integer id) {
        return emballageService.delete(id);
    }

    /**
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = {"/list"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public EmballagesDTO listEmballages(@RequestParam(value = "page", defaultValue = "0", required = false) int page,
                                    @RequestParam(value = "size", defaultValue = "10", required = false) int size) {
        return emballageService.listEmballages(PageRequest.of(page, size));
    }

}
