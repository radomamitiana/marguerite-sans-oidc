package com.renault.dsb.controller.fileController;

import com.renault.dsb.data.constants.FileName;
import com.renault.dsb.data.dto.ResponseDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.service.fileService.FileService;
import com.renault.dsb.service.setting.SettingService;
import com.renault.dsb.service.utils.CryptoUtil;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.util.StringUtils;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;

@RestController
@RequestMapping(value = "/api/fileController")
public class FileController {

    @Autowired
    private FileService dslprService;

    @Autowired
    private SettingService settingService;

    /**
     * download dslpr File
     *
     * @return Resource
     */
    @GetMapping(path = "/download", params = {"type"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseEntity<Resource> download(@RequestParam(value = "type") String type,
                                             @RequestParam(value = "ipn") String currentIpn, @RequestParam(value = "project") String project) {

        SettingDTO settingIpn = null;
        SettingDTO settingPassword = null;
        SettingDTO settingPath = null;

        switch (type) {
            case FileName.CAUSE_ALE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.caledev.path");
                break;
            case FileName.CORRESP_ISO_CODEPAYS:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.codepays.path");
                break;
            case FileName.CORRESPONDANCES:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.tb_corresp.path");
                break;
            case FileName.FACTOR_CAISSE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.load_factor_caisse.path");
                break;
            case FileName.FACTOR_COMMERCE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.load_factor_commerce.path");
                break;
            case FileName.PRIX_MOYEN:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.table_prix_moyen.path");
                break;
            case FileName.ROUTE_REF_COST:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.route_ref_cost.path");
                break;
            case FileName.TARIF_TRONCON:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.tarif_troncon.path");
                break;
            case FileName.TARIF_OP:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.tarif_op.path");
                break;
            case FileName.MODELE_EURO_KM:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.modele_euro_km.path");
                break;
            case FileName.COMPTE_RATTACHEMENT:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.compte_rattachement.path");
                break;
            case FileName.BASE_COMPTES_FRANCE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.base_comptes_france.path");
                break;
            default:
                break;
        }

        if (settingIpn != null && !StringUtils.isEmpty(settingIpn.getValue()) && settingPassword != null
                && !StringUtils.isEmpty(settingPassword.getValue()) && settingPath != null
                && !StringUtils.isEmpty(settingPath.getValue())) {
            CryptoUtil cryptoUtil = new CryptoUtil();
            Resource file = dslprService.download(settingIpn.getValue(), cryptoUtil.decrypt(settingPassword.getValue()),
                    settingPath.getValue(), type, currentIpn, project);
            return ResponseEntity.ok()
                    .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                    .body(file);
        }
        return null;
    }

    /**
     * upload dslpr file
     *
     * @return ResponseDTO
     */
    @PostMapping(path = "/upload", params = {"type"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public ResponseDTO upload(@RequestParam(value = "type") String type,
                              @RequestParam("file") MultipartFile multipartFile, @RequestParam(value = "ipn") String currentIpn) {

        SettingDTO settingIpn = null;
        SettingDTO settingPassword = null;
        SettingDTO settingPath = null;

        switch (type) {
            case FileName.CAUSE_ALE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.caledev.path");
                break;
            case FileName.CORRESP_ISO_CODEPAYS:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.codepays.path");
                break;
            case FileName.CORRESPONDANCES:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.tb_corresp.path");
                break;
            case FileName.FACTOR_CAISSE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.load_factor_caisse.path");
                break;
            case FileName.FACTOR_COMMERCE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.load_factor_commerce.path");
                break;
            case FileName.PRIX_MOYEN:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.table_prix_moyen.path");
                break;
            case FileName.ROUTE_REF_COST:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.route_ref_cost.path");
                break;
            case FileName.TARIF_TRONCON:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.tarif_troncon.path");
                break;
            case FileName.TARIF_OP:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.tarif_op.path");
                break;
            case FileName.MODELE_EURO_KM:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.modele_euro_km.path");
                break;
            case FileName.COMPTE_RATTACHEMENT:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.compte_rattachement.path");
                break;
            case FileName.BASE_COMPTES_FRANCE:
                settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
                settingPassword = settingService.getValueByName("hadoop.dsb.password");
                settingPath = settingService.getValueByName("hadoop.dsb.dslpr.base_comptes_france.path");
                break;
            default:
                break;
        }

        if (settingIpn != null && !StringUtils.isEmpty(settingIpn.getValue()) && settingPassword != null
                && !StringUtils.isEmpty(settingPassword.getValue()) && settingPath != null
                && !StringUtils.isEmpty(settingPath.getValue())) {
            CryptoUtil cryptoUtil = new CryptoUtil();
            return dslprService.upload(settingIpn.getValue(), cryptoUtil.decrypt(settingPassword.getValue()),
                    settingPath.getValue(), multipartFile, type, currentIpn);
        }
        return null;
    }

    /**
     * Download error report endpoint
     *
     * @return file
     */
    @GetMapping(path = "/downloadErrorReport/{errorReport}")
    public ResponseEntity<Resource> downloadErrorReport(@PathVariable("errorReport") String errorReport) {
        Resource file = dslprService.downloadErrorReport(errorReport);
        return ResponseEntity.ok()
                .header(HttpHeaders.CONTENT_DISPOSITION, "attachment; filename=\"" + file.getFilename() + "\"")
                .body(file);
    }
}
