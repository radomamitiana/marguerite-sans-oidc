package com.renault.dsb.controller.user;

import com.renault.dsb.data.dto.AccountInformationDTO;
import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.UserDTO;
import com.renault.dsb.data.dto.UsersDTO;
import com.renault.dsb.service.user.UserService;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(value = "/api/user")
public class UserController {
    @Autowired
    private UserService userService;

    /**
     * @param userDTO
     * @return
     */
    @PostMapping(value = {"/add"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public UserDTO saveUser(@Valid @RequestBody UserDTO userDTO) {
        return userService.create(userDTO);

    }

    /**
     * @param userDTO
     * @return
     */
    @PutMapping(path = "/update")
    public UserDTO updateUser(@RequestBody UserDTO userDTO) {

        return userService.update(userDTO);
    }

    /**
     * @param id
     * @return
     */
    @DeleteMapping(path = "/delete", params = {"id"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public IsDeletedDTO deleteUser(@RequestParam(value = "id") Integer id) {
        return userService.deleteUser(id);
    }

    /**
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = {"/list"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public UsersDTO listUsers(@RequestParam(value = "page", defaultValue = "0", required = false) int page,
                              @RequestParam(value = "size", defaultValue = "10", required = false) int size) {
        return userService.listUsers(PageRequest.of(page, size));
    }

    /**
     * @param id
     * @return
     */
    @GetMapping(path = "/{id}/get")
    public UserDTO getUser(@PathVariable("id") Integer id) {
        return userService.findById(id);
    }

    /**
     * search user endpoint
     *
     * @param stringQuery
     * @param page
     * @param size
     * @return
     */
    @GetMapping(value = {"/list"}, params = {"stringQuery"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public UsersDTO searchUsers(@RequestParam(value = "stringQuery", required = false) String stringQuery,
                                @RequestParam(value = "page", defaultValue = "0", required = false) int page,
                                @RequestParam(value = "size", defaultValue = "10", required = false) int size) {
        return userService.listUsers(stringQuery, PageRequest.of(page, size));
    }

    /**
     * retrieve account info by ipn
     *
     * @param ipn
     * @return
     */
    @GetMapping(value = {"/{ipn}/roles"}, produces = {MediaType.APPLICATION_JSON_VALUE})
    public AccountInformationDTO getAccountInformationByIpn(@PathVariable("ipn") String ipn) {
        return userService.getAccountInformationByIpn(ipn);
    }
}
