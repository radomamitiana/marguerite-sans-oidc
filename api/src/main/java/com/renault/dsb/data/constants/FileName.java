package com.renault.dsb.data.constants;

public class FileName {
    // ====================DSLPR File Name =====================//
    public static final String CAUSE_ALE = "cause_ale";
    public static final String FACTOR_CAISSE = "factor_caisse";
    public static final String FACTOR_COMMERCE = "factor_commerce";
    public static final String CORRESP_ISO_CODEPAYS = "corresp_iso_codepays";
    public static final String CORRESPONDANCES = "correspondances";
    public static final String PRIX_MOYEN = "prix_moyen";
    public static final String ROUTE_REF_COST = "route_ref_cost";
    public static final String TARIF_TRONCON = "tarif_troncon";
    public static final String TARIF_OP = "tarif_op";
    public static final String MODELE_EURO_KM = "modele_euro_km";
    public static final String COMPTE_RATTACHEMENT = "compte_rattachement";
    public static final String BASE_COMPTES_FRANCE = "base_comptes_france";


    // =================== Monitoring File Name ====================//
    public static final String MONITORING = "monitoring";
}
