package com.renault.dsb.data.constants;

public class ProjectType {
	public static final String DSLPR = "dslpr";
	public static final String MONITORING = "monitoring";
	public static final String CO2 = "co2";
	public static final String TOP_ALLIANCE = "top_alliance";

}
