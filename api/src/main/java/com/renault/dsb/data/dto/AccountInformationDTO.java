package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;

@Getter
@Setter
public class AccountInformationDTO {
    private List<String> roles;
    private Boolean isAdmin = false;
}
