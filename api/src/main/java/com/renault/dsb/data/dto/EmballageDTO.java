package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class EmballageDTO {
    private Integer id;
    private Double volume;
    private Double quantite;
    private String entreprise;
    private LocalDateTime dateDepart;
    private LocalDateTime dateArrive;
}
