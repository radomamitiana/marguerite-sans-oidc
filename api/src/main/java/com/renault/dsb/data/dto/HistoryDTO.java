package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
public class HistoryDTO {
	private Integer id;
	private String ipn;
	private String action;
	private String state;
	private LocalDateTime date;
	private int sizeAddLines;
	private String errorReport;
	private boolean hasError;
	private String createdDate;
	private String type;
}
