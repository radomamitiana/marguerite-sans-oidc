package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class IsDeletedDTO {
	private Boolean isDeleted = false;
}
