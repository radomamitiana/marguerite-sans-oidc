package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class MonitoringDTO {
	private Integer id;
	private String base;
	private String tableName;
	private String mini;
	private String maxi;
	private String flagSqlQuery;
	private String commentaire;
	private String cron;
	private String equipe;

}
