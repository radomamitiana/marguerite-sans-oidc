package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;
import org.springframework.data.domain.Page;

@Getter
@Setter
public class MonitoringsDTO {
    private Page<MonitoringDTO> monitoringDTOS;
}
