package com.renault.dsb.data.dto;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.renault.dsb.data.entity.marguerite.RoleName;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class RoleDTO {
    private Integer id;
    @JsonIgnore
    private RoleName name;
}
