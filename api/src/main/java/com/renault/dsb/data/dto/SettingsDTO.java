package com.renault.dsb.data.dto;

import org.springframework.data.domain.Page;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SettingsDTO {
	private Page<SettingDTO> settingDTOS;
}
