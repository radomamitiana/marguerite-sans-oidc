package com.renault.dsb.data.dto;

import lombok.Getter;
import lombok.Setter;

import java.util.List;
import java.util.Set;

@Getter
@Setter
public class UserDTO {
    private Integer id;
    private String ipn;
    private Set<String> strRoles;
    private DropdownListDTO dropdownListDTO;
    private List<DropdownDataDTO> dropdownDataDTOS;
}
