package com.renault.dsb.data.entity.dsb;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "emballage")
public class Emballage {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer id;
    private Double volume;
    private Double quantite;
    private String entreprise;
    private LocalDateTime dateDepart;
    private LocalDateTime dateArrive;
}
