package com.renault.dsb.data.entity.marguerite;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

import java.time.LocalDateTime;

@Getter
@Setter
@Entity
@Table(name = "history")
public class History {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String ipn;
	private String action;
	private String state;
	private LocalDateTime date;
	private int sizeAddLines;
	private String errorReport;
	private boolean hasError;
	private String createdDate;
	private String type;

}
