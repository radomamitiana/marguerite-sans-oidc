package com.renault.dsb.data.entity.marguerite;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "monitoring")
public class Monitoring {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Integer id;
	private String base;
	private String tableName;
	private String mini;
	private String maxi;
	private String flagSqlQuery;
	private String commentaire;
	private String cron;
	private String equipe;

}
