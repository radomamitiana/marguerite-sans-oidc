package com.renault.dsb.mapper;

import com.renault.dsb.data.dto.EmballageDTO;
import com.renault.dsb.data.entity.dsb.Emballage;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;

@Mapper
public interface EmballageMapper {
	EmballageDTO emballageToEmballageDTO(Emballage emballage);

	Emballage emballageDTOToEmballage(EmballageDTO emballageDTO);

	List<EmballageDTO> emballagesToEmballagesDTO(List<Emballage> emballages);

	List<Emballage> emballagesDTOToEmballages(List<EmballageDTO> emballagesDTO);

	default Page<EmballageDTO> emballagePageToEmballageDtoPage(Page<Emballage> emballagePage, Pageable pageable) {
		List<EmballageDTO> emballageDtos = emballagesToEmballagesDTO(emballagePage.getContent());
		Page<EmballageDTO> emballageDTOPage = new PageImpl<>(emballageDtos, pageable, emballagePage.getTotalElements());

		return emballageDTOPage;
	}

}
