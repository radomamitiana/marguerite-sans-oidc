package com.renault.dsb.mapper;

import java.util.List;

import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.data.entity.marguerite.History;
import org.mapstruct.Mapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@Mapper
public interface HistoryMapper {
	
	HistoryDTO historyToHistoryDTO(History history);

	History historyDTOToHistory(HistoryDTO historyDslprDTO);

	List<HistoryDTO> historysToHistorysDTO(List<History> historys);

	List<History> historysDTOToHistorys(List<HistoryDTO> historysDTO);

	default Page<HistoryDTO> historyPageToHistoryDtoPage(Page<History> historyPage, Pageable pageable) {
		List<HistoryDTO> historyDslprDtos = historysToHistorysDTO(historyPage.getContent());
		Page<HistoryDTO> historyDTOPage = new PageImpl<>(historyDslprDtos, pageable, historyPage.getTotalElements());

		return historyDTOPage;
	}

}
