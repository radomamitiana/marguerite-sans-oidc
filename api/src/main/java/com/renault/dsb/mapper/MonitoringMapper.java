package com.renault.dsb.mapper;

import java.util.List;

import org.mapstruct.Mapper;

import com.renault.dsb.data.dto.MonitoringDTO;
import com.renault.dsb.data.entity.marguerite.Monitoring;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

@Mapper
public interface MonitoringMapper {
    MonitoringDTO monitoringToMonitoringDTO(Monitoring monitoring);

    Monitoring monitoringDTOToMonitoring(MonitoringDTO monitoring);

    List<MonitoringDTO> monitoringsToMonitoringDTO(List<Monitoring> monitoring);

    List<Monitoring> monitoringDTOSToMonitoring(List<MonitoringDTO> monitoring);

    default Page<MonitoringDTO> monitoringPageToMonitoringDTOPage(Page<Monitoring> monitoringPage, Pageable pageable) {
        List<MonitoringDTO> monitoringDTOS = monitoringsToMonitoringDTO(monitoringPage.getContent());
        Page<MonitoringDTO> monitoringDTOPage = new PageImpl<>(monitoringDTOS, pageable, monitoringPage.getTotalElements());

        return monitoringDTOPage;

    }

}
