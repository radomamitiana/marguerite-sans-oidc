package com.renault.dsb.repository.dsb;

import com.renault.dsb.data.entity.dsb.Emballage;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EmballageRepository  extends JpaRepository<Emballage, Integer> {
}
