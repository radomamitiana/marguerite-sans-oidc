package com.renault.dsb.repository.marguerite;

import com.renault.dsb.data.entity.marguerite.History;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface HistoryRepository extends JpaRepository<History, Integer> {

	List<History> findByIpnLikeOrActionLikeOrStateLikeOrCreatedDateLikeOrderByIdDesc(String ipn, String action,
			String state, String date);

	Page<History> findByTypeOrderByIdDesc(String type, Pageable pageable);
}
