package com.renault.dsb.repository.marguerite;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import com.renault.dsb.data.entity.marguerite.Monitoring;
import org.springframework.stereotype.Repository;

@Repository
public interface MonitoringRepository extends JpaRepository<Monitoring, Integer> {

    Page<Monitoring> findAll(Pageable pageable);
}
