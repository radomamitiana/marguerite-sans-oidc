package com.renault.dsb.repository.marguerite;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.renault.dsb.data.entity.marguerite.Role;
import com.renault.dsb.data.entity.marguerite.RoleName;

import java.util.Optional;

@Repository
public interface RoleRepository extends JpaRepository<Role, Integer> {
	Optional<Role> findByName(RoleName roleName);
}
