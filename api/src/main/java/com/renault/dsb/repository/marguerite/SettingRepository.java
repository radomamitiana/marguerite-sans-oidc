package com.renault.dsb.repository.marguerite;

import com.renault.dsb.data.entity.marguerite.Setting;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface SettingRepository extends JpaRepository<Setting, Integer> {

	List<Setting> findByName(String name);
}
