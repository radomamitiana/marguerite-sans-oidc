package com.renault.dsb.repository.marguerite;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.renault.dsb.data.entity.marguerite.User;

import java.util.List;

@Repository
public interface UserRepository extends JpaRepository<User, Integer> {
	 List<User> findByIpn(String ipn);
}
