package com.renault.dsb.service.MonitoringService;


import com.renault.dsb.data.dto.MonitoringDTO;
import com.renault.dsb.data.dto.MonitoringsDTO;
import com.renault.dsb.data.dto.ResponseDTO;

import org.springframework.data.domain.Pageable;

public interface MonitoringService {

    MonitoringDTO save(MonitoringDTO monitoringDTO);

    ResponseDTO download();

    ResponseDTO insertInto();

    MonitoringsDTO listMonitoring(Pageable pageable);

}
