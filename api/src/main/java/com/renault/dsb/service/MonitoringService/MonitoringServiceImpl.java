package com.renault.dsb.service.MonitoringService;

import java.io.*;
import java.nio.file.Files;
import java.util.List;
import java.util.stream.Collectors;

import com.renault.dsb.data.dto.MonitoringsDTO;
import com.renault.dsb.data.entity.marguerite.Monitoring;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.renault.dsb.data.dto.MonitoringDTO;
import com.renault.dsb.data.dto.ResponseDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.mapper.MonitoringMapper;
import com.renault.dsb.repository.marguerite.MonitoringRepository;
import com.renault.dsb.service.setting.SettingService;
import com.renault.dsb.service.utils.CryptoUtil;
import com.renault.dsb.service.utils.UtilService;

@Service
public class MonitoringServiceImpl implements MonitoringService, UtilService {

    @Autowired
    private MonitoringMapper monitoringFactory;

    @Autowired
    private MonitoringRepository monitoringRepository;

    @Autowired
    private SettingService settingService;

    @Override
    public MonitoringDTO save(MonitoringDTO monitoringDTO) {
        return monitoringFactory.monitoringToMonitoringDTO(
                monitoringRepository.save(monitoringFactory.monitoringDTOToMonitoring(monitoringDTO)));
    }

    /**
     * Download the input file from the datalake to PC
     *
     * @return
     */
    @Override
    public ResponseDTO download() {
        SettingDTO settingIpn = settingService.getValueByName("hadoop.dsb.ipn");
        SettingDTO settingPassword = settingService.getValueByName("hadoop.dsb.password");
        SettingDTO settingPath = settingService.getValueByName("hadoop.dsb.monitoring.path");

        CryptoUtil cryptoUtil = new CryptoUtil();

        clearAll();
        Runtime runtime = Runtime.getRuntime();
        ResponseDTO responseDTO = new ResponseDTO();
        String filename = "input.csv";
        Process process = null;

        StringBuilder commandLineBuilder = new StringBuilder();
        commandLineBuilder.append("curl -L -k -u ");
        commandLineBuilder.append(settingIpn.getValue());
        commandLineBuilder.append(":");
        commandLineBuilder.append(cryptoUtil.decrypt(settingPassword.getValue()));
        commandLineBuilder.append(" -X ");
        commandLineBuilder.append("GET ");
        commandLineBuilder.append(settingPath.getValue());
        commandLineBuilder.append("?op=OPEN");

        try {
            process = runtime.exec(commandLineBuilder.toString());
            String line;
            BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));
            FileWriter fileWriter = new FileWriter(filename);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            while ((line = output.readLine()) != null) {
                writer.write(line + "\n");
            }
            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        process.destroy();

        responseDTO.setOK(true);

        return responseDTO;
    }

    /**
     * Insert line per line the data on the database
     *
     * @return
     */
    @Override
    public ResponseDTO insertInto() {
        ResponseDTO responseDTO = new ResponseDTO();
        responseDTO = download();
        MonitoringDTO monitoringDTO = new MonitoringDTO();
        String fileName = "input.csv";
        String base = "", tableName = "", comment = "", cron = "", equipe = "", mini = "", maxi = "", flagSql = "";
        int index = 1, i = 1;
        if (responseDTO.isOK()) {
            try {
                List<String> lines = Files.readAllLines(new File(fileName).toPath());
                responseDTO.setOK(true);
                for (String line : lines) {
                    String[] result = line.split(";");
                    for (String s : result) {
                        if (i == 1 && !StringUtils.isEmpty(s)) {
                            base = s;
                        }
                        if (i == 2 && !StringUtils.isEmpty(s)) {
                            tableName = s;
                        }
                        if (i == 3 && !StringUtils.isEmpty(s)) {
                            mini = s;
                        }
                        if (i == 4 && !StringUtils.isEmpty(s)) {
                            maxi = s;
                        }
                        if (i == 5 && !StringUtils.isEmpty(s)) {
                            flagSql = s;
                        }
                        if (i == 6 && !StringUtils.isEmpty(s)) {
                            comment = s;
                        }
                        if (i == 7 && !StringUtils.isEmpty(s)) {
                            cron = s;
                        }
                        if (i == 8 && !StringUtils.isEmpty(s)) {
                            equipe = s;
                        }

                        i++;
                    }

                    monitoringDTO.setBase(base);
                    monitoringDTO.setTableName(tableName);
                    monitoringDTO.setMini(mini);
                    monitoringDTO.setMaxi(maxi);
                    monitoringDTO.setFlagSqlQuery(flagSql);
                    monitoringDTO.setCommentaire(comment);
                    monitoringDTO.setCron(cron);
                    monitoringDTO.setEquipe(equipe);

                    // save to the database
                    this.save(monitoringDTO);
                    index++;
                    i = 1;
                }

            } catch (IOException e) {
                System.err.println("Error -- " + e.toString());
            }
        }

        System.out.println("Insertion OK");
        return responseDTO;
    }

    @Override
    public MonitoringsDTO listMonitoring(Pageable pageable) {
        MonitoringsDTO monitoringsDTO = new MonitoringsDTO();

        Page<Monitoring> monitoringPage = monitoringRepository.findAll(pageable);
        Page<MonitoringDTO> monitoringDTOPage = monitoringFactory.monitoringPageToMonitoringDTOPage(monitoringPage, pageable);

        if (!StringUtils.isEmpty(monitoringDTOPage)) {
            List<MonitoringDTO> monitoringDTOList = monitoringDTOPage.getContent().stream().collect(Collectors.toList());

            Page<MonitoringDTO> monitoringsDTOPage = new PageImpl<>(monitoringDTOList, monitoringPage.getPageable(), monitoringPage.getTotalElements());

            monitoringsDTO.setMonitoringDTOS(monitoringsDTOPage);
        }
        return monitoringsDTO;
    }

}
