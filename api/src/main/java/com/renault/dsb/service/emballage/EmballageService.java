package com.renault.dsb.service.emballage;

import com.renault.dsb.data.dto.EmballageDTO;
import com.renault.dsb.data.dto.EmballagesDTO;
import com.renault.dsb.data.dto.IsDeletedDTO;
import org.springframework.data.domain.Pageable;

public interface EmballageService {
    EmballageDTO createOrUpdate(EmballageDTO emballageDTO);

    IsDeletedDTO delete(Integer id);

    EmballagesDTO listEmballages(Pageable pageable);
}
