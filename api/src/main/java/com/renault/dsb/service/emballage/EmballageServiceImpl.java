package com.renault.dsb.service.emballage;

import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.EmballageDTO;
import com.renault.dsb.data.dto.EmballagesDTO;
import com.renault.dsb.data.entity.dsb.Emballage;
import com.renault.dsb.mapper.EmballageMapper;
import com.renault.dsb.repository.dsb.EmballageRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

@Service
public class EmballageServiceImpl  implements EmballageService {

    @Autowired
    private EmballageRepository emballageRepository;

    @Autowired
    private EmballageMapper emballageFactory;

    @Override
    public EmballageDTO createOrUpdate(EmballageDTO emballageDTO) {

        return emballageFactory
                .emballageToEmballageDTO(emballageRepository.save(emballageFactory.emballageDTOToEmballage(emballageDTO)));
    }

    @Override
    public IsDeletedDTO delete(Integer id) {
        IsDeletedDTO isDeletedDTO = new IsDeletedDTO();
        Emballage emballage = emballageRepository.findById(id).orElse(null);
        if (emballage != null) {
            emballageRepository.delete(emballage);
            isDeletedDTO.setIsDeleted(true);
        }
        return isDeletedDTO;
    }

    /**
     * List emballage
     */
    @Override
    public EmballagesDTO listEmballages(Pageable pageable) {
        EmballagesDTO emballagesDTO = new EmballagesDTO();

        emballagesDTO.setEmballageDTOS(emballageFactory.emballagePageToEmballageDtoPage(emballageRepository.findAll(pageable), pageable));

        return emballagesDTO;
    }
}
