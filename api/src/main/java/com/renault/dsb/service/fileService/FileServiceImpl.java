package com.renault.dsb.service.fileService;

import java.io.*;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;

import com.renault.dsb.data.constants.FileName;
import com.renault.dsb.data.constants.ProjectType;
import com.renault.dsb.data.dto.HistoryDTO;
import org.apache.commons.io.FilenameUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;
import org.springframework.web.multipart.MultipartFile;

import com.renault.dsb.data.dto.ResponseDTO;
import com.renault.dsb.service.history.HistoryService;
import com.renault.dsb.service.utils.UtilService;

@Service
public class FileServiceImpl implements FileService, UtilService, VerifyFileService {

    private final static Logger logger = LoggerFactory.getLogger(FileService.class);

    @Autowired
    private HistoryService historyService;

    /**
     * download file from datalake to local
     *
     * @return
     */
    @Override
    public Resource download(String ipn, String password, String path, String type, String currentIpn, String project) {
        clearAll();
        Runtime runtime = Runtime.getRuntime();
        ResponseDTO responseDTO = new ResponseDTO();
        String filename = null;
        Process process = null;

        StringBuilder commandLineBuilder = new StringBuilder();
        commandLineBuilder.append("curl -L -k -u ");
        commandLineBuilder.append(ipn);
        commandLineBuilder.append(":");
        commandLineBuilder.append(password);
        commandLineBuilder.append(" -X ");
        commandLineBuilder.append("GET ");
        commandLineBuilder.append(path);
        commandLineBuilder.append("?op=OPEN");

        try {
            process = runtime.exec(commandLineBuilder.toString());
            String line;
            BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));

            switch (project) {
                case ProjectType.DSLPR:
                    filename = "dslpr.csv";
                    break;
                case ProjectType.MONITORING:
                    filename = "monitoring.csv";
                    break;
                case ProjectType.CO2:
                    filename = "co2.csv";
                    break;
                case ProjectType.TOP_ALLIANCE:
                    filename = "top_alliance.csv";
                    break;
                default:
                    break;
            }

            FileWriter fileWriter = new FileWriter("storage/" + filename);
            BufferedWriter writer = new BufferedWriter(fileWriter);
            while ((line = output.readLine()) != null) {

                writer.write(line + "\n");
            }

            writer.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        process.destroy();

        // check if file is empty or no
        File f = load(filename).toFile();
        System.out.println("size: " + f.length());
        if (f.exists() && f.length() == 0) {
            responseDTO.setOK(false);
            logger.info("Download KO");

            HistoryDTO historyDTO = new HistoryDTO();
            historyDTO.setIpn(currentIpn);
            historyDTO.setAction("Download");
            historyDTO.setState("KO");
            historyDTO.setHasError(false);
            historyDTO.setType(type);
            historyService.save(historyDTO);
        } else {
            responseDTO.setOK(true);
            logger.info("Download successfull");

            HistoryDTO historyDTO = new HistoryDTO();
            historyDTO.setIpn(currentIpn);
            historyDTO.setAction("Download");
            historyDTO.setState("OK");
            historyDTO.setHasError(false);
            historyDTO.setType(type);
            historyService.save(historyDTO);
        }

        System.out.println("result : " + responseDTO.isOK());
        return loadAsResource(filename);
    }

    /**
     * Upload file from local to the datalake
     *
     * @param ipn
     * @return
     */
    @Override
    public ResponseDTO upload(String ipn, String password, String path, MultipartFile file, String type,
                              String currentIpn) {
        clearAll();
        Runtime runtime = Runtime.getRuntime();
        ResponseDTO responseDTO = new ResponseDTO();
        File fileToUpload = null;
        boolean isOk;

        try {
            fileToUpload = convert(file);
        } catch (IOException ex) {
            logger.error(ex.getMessage());
        }

        String fileToUploadName = store(file);

        isOk = verify(type, file.getOriginalFilename(), fileToUpload, currentIpn);

        if (isOk) {
            try {
                StringBuilder commandLineBuilder = new StringBuilder();
                if (type.equals("monitoring")) {
                    int day = LocalDateTime.now().getDayOfMonth();
                    int month = LocalDateTime.now().getMonthValue();
                    int year = LocalDateTime.now().getYear();
                    path = path + "input_" + year + "-" + month + "-" + day;
                }

                // if the file control is OK, delete file on datalake and uplaod the new file
                clearData(runtime, ipn, password, path);

                commandLineBuilder.append("curl -L -k -u ");
                commandLineBuilder.append(ipn);
                commandLineBuilder.append(":");
                commandLineBuilder.append(password);
                commandLineBuilder.append(" -X ");
                commandLineBuilder.append("PUT ");
                commandLineBuilder.append(path);
                commandLineBuilder.append("?op=create ");
                commandLineBuilder.append("-H 'Content-Type: applicaiton/text' -T ");
                commandLineBuilder.append("storage/" + fileToUploadName);

                Process process = runtime.exec(commandLineBuilder.toString());

                String line;
                BufferedReader output = new BufferedReader(new InputStreamReader(process.getInputStream()));

                while ((line = output.readLine()) != null) {
                    logger.info(line);
                }
                output.close();
                process.destroy();
                responseDTO.setOK(true);
                logger.info("Upload successfull");

            } catch (IOException ex) {
                logger.error(ex.getMessage());
            }
        }

        fileToUpload.delete();
        return responseDTO;

    }

    /**
     * download report file error
     */
    @Override
    public Resource downloadErrorReport(String errorReport) {
        return loadAsResourceError(errorReport);
    }

    /**
     * controle file before to send to the datalake
     *
     * @param fileName
     * @return
     */
    private boolean verify(String type, String fileName, File fileToUpload, String currentIpn) {
        String extension = FilenameUtils.getExtension(fileName);
        int count = 0;
        boolean hasError = false;
        StringBuilder stringBuilderErrorFileName = new StringBuilder();

        // Control if is it a text file and the file is not empty
        if (!StringUtils.isEmpty(extension) && extension.equals("csv") && fileToUpload.length() > 0) {

            try {
                List<String> allLines = Files.readAllLines(fileToUpload.toPath());
                count = contNumberOfLine(allLines);
                int day = LocalDateTime.now().getDayOfMonth();
                int month = LocalDateTime.now().getMonthValue();
                int year = LocalDateTime.now().getYear();
                int hour = LocalDateTime.now().getHour();
                int minute = LocalDateTime.now().getMinute();
                int second = LocalDateTime.now().getSecond();

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");

                stringBuilderErrorFileName.append("error_report_" + day);
                stringBuilderErrorFileName.append("-" + month);
                stringBuilderErrorFileName.append("-" + year);
                stringBuilderErrorFileName.append("_" + hour);
                stringBuilderErrorFileName.append("_" + minute);
                stringBuilderErrorFileName.append("_" + second);
                stringBuilderErrorFileName.append(".txt");

                // Build the file error report
                BufferedWriter writer = new BufferedWriter(
                        new FileWriter("file/" + stringBuilderErrorFileName.toString()));
                StringBuilder stringBuilderReportTitle = new StringBuilder();
                stringBuilderReportTitle.append("Action : Upload file\n\n");
                stringBuilderReportTitle.append("File name : ");
                stringBuilderReportTitle.append(fileName + "\n\n");
                stringBuilderReportTitle.append("Date : ");
                stringBuilderReportTitle.append(dtf.format(LocalDateTime.now()).toString() + "\n\n");
                stringBuilderReportTitle.append("User : ");

                stringBuilderReportTitle.append(currentIpn);
                writer.write(stringBuilderReportTitle.toString() + "\n\n");
                writer.write("Result of upload : Errors found  - File rejected\n\n");
                writer.write("Errors :\n");

                switch (type) {
                    case FileName.CAUSE_ALE:
                        hasError = verifyFileCauseAle(writer, allLines, hasError);
                        break;
                    case FileName.CORRESP_ISO_CODEPAYS:
                        hasError = verifyColumn(2, writer, allLines, hasError);
                        break;
                    case FileName.CORRESPONDANCES:
                        hasError = verifyFileTableCorrespondances(writer, allLines, hasError);
                        break;
                    case FileName.FACTOR_CAISSE:
                        hasError = verifyFileLoadFactorCaisse(writer, allLines, hasError);
                        break;
                    case FileName.FACTOR_COMMERCE:
                        hasError = verifyFileFactorCommerce(writer, allLines, hasError);
                        break;
                    case FileName.PRIX_MOYEN:
                        hasError = verifyFilePrixMoyen(writer, allLines, hasError);
                        break;
                    case FileName.ROUTE_REF_COST:
                        hasError = verifyFileRouteRefCost(writer, allLines, hasError);
                        break;
                    case FileName.TARIF_TRONCON:
                        hasError = verifyFileTarifTrancon(writer, allLines, hasError);
                        break;
                    case FileName.TARIF_OP:
                        hasError = verifyFileTarifOp(writer, allLines, hasError);
                        break;
                    case FileName.MODELE_EURO_KM:
                        hasError = verifyFileModeleEuroKm(writer, allLines, hasError);
                        break;
                    case FileName.COMPTE_RATTACHEMENT:
                        hasError = verifyFileCompteRattachement(writer, allLines, hasError);
                        break;
                    case FileName.BASE_COMPTES_FRANCE:
                        hasError = verifyFileBaseComptesFrance(writer, allLines, hasError);
                        break;
                    default:
                        break;
                }

                writer.close();
            } catch (IOException e) {
                System.err.println("Error -- " + e.toString());
            }
        } else {
            try {

                DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy HH:mm:ss");
                stringBuilderErrorFileName.append("error_file_extension");
                stringBuilderErrorFileName.append(".txt");

                BufferedWriter writer = new BufferedWriter(
                        new FileWriter("file/" + stringBuilderErrorFileName.toString()));
                StringBuilder stringBuilderReportTitle = new StringBuilder();
                stringBuilderReportTitle.append("Action : Upload file\n\n");
                stringBuilderReportTitle.append("File name : ");
                stringBuilderReportTitle.append(fileName + "\n\n");
                stringBuilderReportTitle.append("Date : ");
                stringBuilderReportTitle.append(dtf.format(LocalDateTime.now()).toString() + "\n\n");
                stringBuilderReportTitle.append("User : ");
                stringBuilderReportTitle.append(currentIpn);
                writer.write(stringBuilderReportTitle.toString() + "\n\n");
                writer.write("Result of upload : Errors found  - File rejected\n\n");
                writer.write("Errors :\n");
                writer.write("File error. This is not a csv file or the file is empty");
                writer.close();
                hasError = true;

            } catch (IOException e) {
                System.err.println("Error -- " + e.toString());
            }

        }

        // Insert a new row in the history table
        HistoryDTO historyDTO = new HistoryDTO();
        historyDTO.setType(type);
        historyDTO.setIpn(currentIpn);
        historyDTO.setAction("Upload");
        historyDTO.setSizeAddLines(count);

        if (!hasError) {
            historyDTO.setHasError(false);
            historyDTO.setState("OK");

            // save history to the database
            historyService.save(historyDTO);
            removeFileReportTmp("file/" + stringBuilderErrorFileName.toString());
            return true;
        }
        historyDTO.setHasError(true);
        historyDTO.setErrorReport(stringBuilderErrorFileName.toString());
        historyDTO.setState("KO");

        // save history to the database
        historyService.save(historyDTO);

        return false;
    }


    /**
     * Convert MultipartFile to File
     *
     * @param file
     * @return
     * @throws IOException
     */
    public static File convert(MultipartFile file) throws IOException {
        File convFile = new File(file.getOriginalFilename());
        convFile.createNewFile();
        FileOutputStream fos = new FileOutputStream(convFile);
        fos.write(file.getBytes());
        fos.close();
        return convFile;
    }

}
