package com.renault.dsb.service.fileService;

import org.springframework.util.StringUtils;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.StringTokenizer;
import java.util.List;

public interface VerifyFileService {

    // =================================== Common functions ========================================//

    /**
     * Verify the value is a decimal with a . for separator
     *
     * @param value
     * @return
     */
    static boolean isTypeDecimal(String value) {
        return value.matches("-?\\d+(\\.\\d+)?");
    }

    /**
     * Verify if the value is an integer
     *
     * @param value
     * @return
     */
    static boolean isInteger(String value) {
        return value.matches("\\d+");
    }

    /**
     * Verify if the value is an integer and the value's length equal 3
     *
     * @param value
     * @return
     */
    static boolean isIntegerAndThreeCaracters(String value) {
        return (isInteger(value) && value.length() == 3);
    }

    /**
     * Count column in the file
     *
     * @param column
     * @param line
     * @param hasError
     * @param index
     * @return
     */
    static boolean countColumn(int column, String line, boolean hasError, int index,
                               BufferedWriter writer) throws IOException {
        if (StringUtils.countOccurrencesOf(line, ";") != column - 1) {
            writer.write("Line " + index + ": The number of column is not " + column + ".\n");
            hasError = true;
        }
        return hasError;
    }

    /**
     * Control if the file contains has right number columns
     *
     * @param column
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyColumn(int column, BufferedWriter writer, List<String> lines, boolean hasError)
            throws IOException {
        int index = 0;
        for (var line : lines) {
            StringTokenizer stringTokenizer = new StringTokenizer(line, ";");
            if (stringTokenizer.countTokens() != column) {
                writer.write("Line " + index + ": The number of column is not " + column + ".\n");
                hasError = true;
            }
            index++;
        }
        return hasError;
    }

    /**
     * Verify if the value is a number decimal and value's length = 9
     *
     * @param value
     * @return
     */
    static boolean isDecimalAndContainNineCharacters(String value) {
        return (isInteger(value) && value.length() == 9);
    }

    // ================================= Functions special for all file in DSLPR project ==================//

    /**
     * Count number of line without the title
     *
     * @param lines
     * @return
     */
    default int contNumberOfLine(List<String> lines) {
        int col = 0;
        for (var line : lines) {
            col++;
        }
        return col - 1;
    }

    /**
     * Verify the file cause_ale_deviation
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileCauseAle(BufferedWriter writer, List<String> lines, boolean hasError) throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(12, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 1 && s.isEmpty()) {
                        writer.write("Line " + index + " : The column VIN is absent.\n");
                        hasError = true;
                    }
                    if (i == 12 && !isInteger(s)) {
                        writer.write("Line " + index + " : The column semaine is not an integer.\n");
                        hasError = true;
                    }
                }
                i++;
            }
            index++;
            i = 1;
        }
        return hasError;
    }

    /**
     * Verify the file Table correspondance
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileTableCorrespondances(BufferedWriter writer, List<String> lines, boolean hasError)
            throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(4, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 1 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + " : The column projet is absent.\n");
                        hasError = true;
                    }
                }
                i++;
            }
            index++;
            i = 1;
        }
        return hasError;
    }

    /**
     * Verify the file load factor caisse
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileLoadFactorCaisse(BufferedWriter writer, List<String> lines, boolean hasError)
            throws IOException {
        return verifyColumnIsAbsentAndOtherColumnMightTypeDecimalOrNull("mod_caisse", writer, lines, hasError);
    }

    /**
     * Verify the file load factor commerce
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileFactorCommerce(BufferedWriter writer, List<String> lines, boolean hasError)
            throws IOException {
        return verifyColumnIsAbsentAndOtherColumnMightTypeDecimalOrNull("mod_commerce", writer, lines, hasError);
    }

    /**
     * Verifiy is the column is absent or not the right type
     *
     * @param column
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    static boolean verifyColumnIsAbsentAndOtherColumnMightTypeDecimalOrNull(String column, BufferedWriter writer,
                                                                            List<String> lines, boolean hasError) throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(4, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 1 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + " : The column " + column + " is absent.\n");
                        hasError = true;
                    }
                    if (i == 2 && !s.equals("NULL") && !isTypeDecimal(s.trim())) {
                        writer.write("Line " + index + " : The column lfCo2 must be a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }
                    if (i == 3 && !s.equals("NULL") && !isTypeDecimal(s.trim())) {
                        writer.write("Line " + index + " : The column lf_reel must be a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }
                    if (i == 4 && !s.equals("NULL") && !isTypeDecimal(s.trim())) {
                        writer.write("Line " + index + " : The column lf_theo must be a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }
                }

                i++;
            }
            index++;
            i = 1;
        }
        return hasError;
    }

    /**
     * Verify File Route_ref_cost
     *
     * @param lines
     * @param hasError
     * @param writer
     * @return
     * @throws IOException
     */
    default boolean verifyFileRouteRefCost(BufferedWriter writer, List<String> lines, boolean hasError)
            throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(63, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 1 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column RR_Numero is absent.\n");
                        hasError = true;
                    }
                    if (i == 2 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column RR_Inutile is absent.\n");
                        hasError = true;
                    }
                    if (i == 3 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column RR_Start_Date is absent.\n");
                        hasError = true;
                    }
                    if (i == 4 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column RR_End_Date is absent.\n");
                        hasError = true;
                    }
                    if (i == 5 && !isIntegerAndThreeCaracters(s)) {
                        writer.write(
                                "Line " + index + ": The column Code_Origine must be an integer and 3 characters (ex: 031).\n");
                        hasError = true;
                    }

                    if (i == 21 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Prix_Net_Euro must be a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }

                    if (i == 37 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column TR_Kilometrage must be an integer.\n");
                        hasError = true;
                    }

                }
                i++;
            }

            index++;
            i = 1;
        }

        return hasError;
    }

    /**
     * Verify the file Prix moyen
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFilePrixMoyen(BufferedWriter writer, List<String> lines, boolean hasError)
            throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            StringTokenizer stringTokenizer = new StringTokenizer(line, ";");
            hasError = countColumn(5, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 1 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column h020codedptmo is absent.\n");
                        hasError = true;
                    }
                    if (i == 2 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column h020codpays is not an integer.\n");
                        hasError = true;
                    }
                    if (i == 3 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column modele is absent.\n");
                        hasError = true;
                    }
                    if (i == 4 && !s.equals("NULL") && !isTypeDecimal(s.trim())) {
                        writer.write("Line " + index + ": The column prix_moyen_usine_pays is not a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }
                    if (i == 5 && !s.equals("NULL") && !isTypeDecimal(s.trim())) {
                        writer.write("Line " + index + ": The column cpc_moyen_usine_pays is not a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }
                }
                i++;
            }
            index++;
            i = 1;
        }

        return hasError;
    }


    /**
     * Verify tarif trancon file
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileTarifTrancon(BufferedWriter writer, List<String> lines, boolean hasError) throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(24, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 4 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column Departure_compound(OTA) is absent.\n");
                        hasError = true;
                    }
                    if (i == 5 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column Arrival_compound is absent.\n");
                        hasError = true;
                    }
                    if (i == 6 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column Steffi_supplier_code(OTA) is absent.\n");
                        hasError = true;
                    }
                    if (i == 13 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column Mode_transport is absent.\n");
                        hasError = true;
                    }
                    if (i == 15 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Prix_net (Euro) must be a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }
                    if (i == 16 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Prix_net (Devise CNT) must be a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }
                    if (i == 18 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Tarif_BP (Devise CNT) must be a number decimal (ex: 35.46).\n");
                        hasError = true;
                    }
                }
                i++;
            }
            index++;
            i = 1;

        }
        return hasError;
    }

    /**
     * Verify Tarif_Op file
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileTarifOp(BufferedWriter writer, List<String> lines, boolean hasError) throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(15, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 1 && !isIntegerAndThreeCaracters(s)) {
                        writer.write("Line " + index + ": The column Code_Origine must be a 3 integer between 0-9. (ex: 001)\n");
                        hasError = true;
                    }
                    if (i == 2 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column ISO is absent.\n");
                        hasError = true;
                    }
                    if (i == 4 && StringUtils.isEmpty(s)) {
                        writer.write("Line " + index + ": The column Centre_de_Passage is absent.\n");
                        hasError = true;
                    }
                    if (i == 5 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Cout_de_passage_centre must be a number decimal (ex : 39.46).\n");
                        hasError = true;
                    }
                    if (i == 10 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Prix_net (Euro) must be a number decimal (ex : 39.46).\n");
                        hasError = true;
                    }
                    if (i == 11 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Prix_net (Devise cnt) must be a number decimal (ex : 39.46).\n");
                        hasError = true;
                    }

                }
                i++;
            }
            index++;
            i = 1;

        }
        return hasError;
    }

    /**
     * Verify Modele_Euro_Km file
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileModeleEuroKm(BufferedWriter writer, List<String> lines, boolean hasError) throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(9, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 1) {
                        if (!StringUtils.isEmpty(s) && !isInteger(s)) {
                            writer.write("Line " + index + ": The column km_30 must be an integer. (ex: 001)\n");
                            hasError = true;
                        } else if (StringUtils.isEmpty(s)) {
                            writer.write("Line " + index + ": The column km_30 is empty.\n");
                            hasError = true;
                        }
                    }
                    if (i == 2) {
                        if (!StringUtils.isEmpty(s) && !isInteger(s)) {
                            writer.write("Line " + index + ": The column km2_30 must be  an integer. (ex: 001)\n");
                            hasError = true;
                        } else if (StringUtils.isEmpty(s)) {
                            writer.write("Line " + index + ": The column km2_30 is empty.\n");
                            hasError = true;
                        }
                    }
                    if (i == 3 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Modele_euro_km_30_rv must be a number decimal (ex : 39.45).\n");
                        hasError = true;
                    }
                    if (i == 4 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column km_100 must be an integer (ex : 45).\n");
                        hasError = true;
                    }
                    if (i == 5 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column km2_100 must be an integer (ex : 45).\n");
                        hasError = true;
                    }
                    if (i == 6 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Modele_euro_km_100_rv must be a number decimal (ex : 39.45).\n");
                        hasError = true;
                    }
                    if (i == 7 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column km_0 must be an integer (ex : 45).\n");
                        hasError = true;
                    }
                    if (i == 8 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column km2_0 must be an integer (ex : 45).\n");
                        hasError = true;
                    }
                    if (i == 6 && !StringUtils.isEmpty(s) && !isTypeDecimal(s)) {
                        writer.write("Line " + index + ": The column Modele_euro_km_0_rv must be a number decimal (ex : 39.45).\n");
                        hasError = true;
                    }
                }
                i++;
            }
            index++;
            i = 1;
        }
        return hasError;
    }

    /**
     * Verify Compte_Rattachement file
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileCompteRattachement(BufferedWriter writer, List<String> lines, boolean hasError) throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(28, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 1 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column Dc_Pro must be an integer(ex : 3945).\n");
                        hasError = true;
                    }
                    if (i == 2 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column Dcz_Pro must be an integer(ex : 35).\n");
                        hasError = true;
                    }
                    if (i == 3 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column Compte_Pro must be an integer(ex : 45).\n");
                        hasError = true;
                    }
                    if (i == 4) {
                        if (!StringUtils.isEmpty(s) && !isDecimalAndContainNineCharacters(s)) {
                            writer.write("Line " + index + ": The column DcDczCpte_Pro must be a 9 integers between 0-9 (ex : 102028700).\n");
                            hasError = true;
                        } else if (StringUtils.isEmpty(s)) {
                            writer.write("Line " + index + ": The column DcDczCpte_Pro is empty.\n");
                            hasError = true;
                        }
                    }
                    if (i == 5 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column DC_dest must be an integer(ex : 35).\n");
                        hasError = true;
                    }
                    if (i == 6 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column Dcz_dest must be an integer (ex : 345).\n");
                        hasError = true;
                    }
                    if (i == 7 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column Compte_Dest must be an integer (ex : 345).\n");
                        hasError = true;
                    }
                    if (i == 8) {
                        if (!StringUtils.isEmpty(s) && !isDecimalAndContainNineCharacters(s)) {
                            writer.write("Line " + index + ": The column DcDczCpte_Dest must be a 9 integers between 0-9 (ex : 102028700).\n");
                            hasError = true;
                        } else if (StringUtils.isEmpty(s)) {
                            writer.write("Line " + index + ": The column DcDczCpte_Dest is empty.\n");
                            hasError = true;
                        }
                    }
                    if (i == 9 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column DcDczCpte_Pro2 must be an integer (ex : 39).\n");
                        hasError = true;
                    }
                    if (i == 10 && !StringUtils.isEmpty(s) && !isInteger(s)) {
                        writer.write("Line " + index + ": The column DcDczCpte_1_et_2 must be an integer (ex : 45).\n");
                        hasError = true;
                    }
                }
                i++;
            }
            index++;
            i = 1;
        }
        return hasError;
    }

    /**
     * Verify file base comptes france
     *
     * @param writer
     * @param lines
     * @param hasError
     * @return
     * @throws IOException
     */
    default boolean verifyFileBaseComptesFrance(BufferedWriter writer, List<String> lines, boolean hasError)
            throws IOException {
        int index = 1, i = 1;
        for (var line : lines) {
            String[] result = line.split(";");
            hasError = countColumn(28, line, hasError, index, writer);
            for (var s : result) {
                if (index > 1) {
                    if (i == 2) {
                        if (!StringUtils.isEmpty(s) && !isInteger(s)) {
                            writer.write("Line " + index + ": The column DC must be an integer.\n");
                            hasError = true;
                        } else if (StringUtils.isEmpty(s)) {
                            writer.write("Line " + index + ": The column DC is empty.\n");
                            hasError = true;
                        }
                    }
                    if (i == 3) {
                        if (!StringUtils.isEmpty(s) && !isInteger(s)) {
                            writer.write("Line " + index + ": The column DCZ must be an integer.\n");
                            hasError = true;
                        } else if (StringUtils.isEmpty(s)) {
                            writer.write("Line " + index + ": The column DCZ is empty.\n");
                            hasError = true;
                        }
                    }
                    if (i == 4) {
                        if (!StringUtils.isEmpty(s) && s.length() != 6) {
                            writer.write("Line " + index + ": The column Compte must be a 6 characters.\n");
                            hasError = true;
                        } else if (StringUtils.isEmpty(s)) {
                            writer.write("Line " + index + ": The column DCZ is empty.\n");
                            hasError = true;
                        }
                    }
                }
                i++;
            }
            index++;
            i = 1;
        }
        return hasError;
    }

}
