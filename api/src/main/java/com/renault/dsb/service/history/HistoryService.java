package com.renault.dsb.service.history;

import org.springframework.data.domain.Pageable;

import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.data.dto.HistorysDTO;

public interface HistoryService {
	HistorysDTO listHistorys(String type, Pageable pageable);

	HistoryDTO save(HistoryDTO historyDslprDTO);

	HistorysDTO listHistorys(String type, String stringQuery, Pageable pageable);
}
