package com.renault.dsb.service.history;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.stream.Collectors;

import com.renault.dsb.data.dto.HistoryDTO;
import com.renault.dsb.data.entity.marguerite.History;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import com.renault.dsb.data.dto.HistorysDTO;
import com.renault.dsb.mapper.HistoryMapper;
import com.renault.dsb.repository.marguerite.HistoryRepository;

@Service
public class HistoryServiceImpl implements HistoryService {

    @Autowired
    private HistoryRepository historyRepository;
    @Autowired
    private HistoryMapper historyFactory;

    /**
     * get story dslpr caus_ale list and filter by date
     */
    @Override
    public HistorysDTO listHistorys(String type, Pageable pageable) {
        HistorysDTO historysDTO = new HistorysDTO();

        Page<History> historyPage = historyRepository.findByTypeOrderByIdDesc(type, pageable);
        Page<HistoryDTO> historyDTOPage = historyFactory.historyPageToHistoryDtoPage(historyPage, pageable);

        if (historyDTOPage != null) {
            List<HistoryDTO> historyDslprDTOList = historyDTOPage.getContent().stream()
                    .collect(Collectors.toList());

            Page<HistoryDTO> historysDTOPage = new PageImpl<>(historyDslprDTOList, historyPage.getPageable(),
                    historyPage.getTotalElements());

            historysDTO.setHistoryDTOS(historysDTOPage);
        }

        return historysDTO;
    }

    /**
     * Save history dslpr file
     */
    @Override
    public HistoryDTO save(HistoryDTO historyDslprDTO) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        historyDslprDTO.setDate(LocalDateTime.now());
        historyDslprDTO.setCreatedDate(dtf.format(historyDslprDTO.getDate()));
        return historyFactory
                .historyToHistoryDTO(historyRepository.save(historyFactory.historyDTOToHistory(historyDslprDTO)));
    }

    /**
     * Search anything in the story dslpr file and filter by date
     */
    @Override
    public HistorysDTO listHistorys(String type, String stringQuery, Pageable pageable) {
        if (StringUtils.isEmpty(stringQuery)) {
            return listHistorys(type, pageable);
        }

        HistorysDTO historysDTO = new HistorysDTO();

        List<History> historyDslprList = historyRepository.findByIpnLikeOrActionLikeOrStateLikeOrCreatedDateLikeOrderByIdDesc(stringQuery, stringQuery,
                stringQuery, stringQuery);

        List<History> historyDslprs = historyDslprList.stream().takeWhile((var x) -> x.getType() != null && x.getType().equals(type))
                .collect(Collectors.toList());
        List<HistoryDTO> historyDslprDTOS = historyFactory.historysToHistorysDTO(historyDslprs);

        int start = (int) pageable.getOffset();
        int end = Math.min((start + pageable.getPageSize()), historyDslprDTOS.size());
        historysDTO.setHistoryDTOS(new PageImpl<>(historyDslprDTOS.subList(start, end), pageable, historyDslprDTOS.size()));

        return historysDTO;
    }

}
