package com.renault.dsb.service.setting;

import com.renault.dsb.data.dto.IsDeletedDTO;
import com.renault.dsb.data.dto.SettingDTO;
import com.renault.dsb.data.dto.SettingsDTO;
import org.springframework.data.domain.Pageable;

public interface SettingService {
	SettingDTO createOrUpdate(SettingDTO settingDTO);

	IsDeletedDTO delete(Integer id);

	SettingsDTO listSettings(Pageable pageable);

	SettingsDTO listSettings(String stringQuery, Pageable pageable);

	SettingDTO getValueByName(String name);

}
