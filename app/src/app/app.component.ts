import { Component } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';
import { AuthenticationService } from './service/authentication/authentication.service';
import { OAuthService, JwksValidationHandler } from 'angular-oauth2-oidc';
import { authConfig } from './auth.config';
import { Router } from '@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {

  userInfoEndpoint: string;

  constructor(private authenticationService: AuthenticationService,
    private translate: TranslateService,
    private oauthService: OAuthService,
    private router: Router) {
    translate.setDefaultLang('fr');
    sessionStorage.setItem('ipn', 'p102818');
  }

  logOut() {
    this.oauthService.logOut();
  }
}
