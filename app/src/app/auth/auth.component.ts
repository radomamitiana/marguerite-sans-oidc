import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { AuthenticationService } from '../service/authentication/authentication.service';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-auth',
  templateUrl: './auth.component.html',
  styleUrls: ['./auth.component.css']
})
export class AuthComponent implements OnInit {
  ipn = '';
  password = '';
  invalidLogin = false;
  notification = '';

  constructor(private router: Router,
    private authService: AuthenticationService, public translate: TranslateService) {
  }

  ngOnInit() {
    if (this.authService.isUserLoggedIn()) {
      this.router.navigate(['']);
    }
  }

  checkLogin() {
    (this.authService.authenticate(this.ipn, this.password).subscribe(
      data => {
        this.router.navigate(['']);
        this.invalidLogin = false;
      },
      error => {
        this.invalidLogin = true;
        this.translate.get('notification.error_login').subscribe((text: string) => {
          this.notification = text;
        });
      }
    )
    );

  }
}
