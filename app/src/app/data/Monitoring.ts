export class Monitoring {
    constructor(
        public id: number,
        public base: string,
        public tableName: string,
        public mini: string,
        public maxi: string,
        public flagSqlQuery: string,
        public commentaire: string,
        public cron: string,
        public equipe: string

    ) { }
}