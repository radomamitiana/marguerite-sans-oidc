import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as fileSaver from 'file-saver';
import { FileService } from '../service/file.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { History } from '../data/History';
import { TranslateService } from '@ngx-translate/core';
import { FileName } from '../utils/FileName';
import { ProjectType } from '../utils/ProjectType';

@Component({
  selector: 'app-dslpr',
  templateUrl: './dslpr.component.html',
  styleUrls: ['./dslpr.component.css']
})
export class DslprComponent implements OnInit {
  selectedFiles: FileList;
  currentFileUpload: File;
  notification = '';
  isLoading = false;
  isOK = false;
  isKO = false;
  historys: History[];
  progress: { percentage: number } = { percentage: 0 };
  stringQuery: string;

  pageEmpty: true;
  totalPages: 1;
  totalPagesArray = [];
  last: false;
  first: false;
  page = 0;
  type = FileName.BASE_COMPTES_FRANCE;
  project = ProjectType.DSLPR;
  title = ' "BASE COMPTES FRANCE"';
  classCauseAle = 'btn btn-default bg-dark col-lg-12';
  classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
  classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
  classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
  classCorespondances = 'btn btn-default  bg-dark col-lg-12';
  classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
  classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
  classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
  classTarifOP = 'btn btn-default  bg-dark col-lg-12';
  classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
  classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
  classBaseComptesFrance = 'btn btn-default  bg-warning col-lg-12';

  typeAlert = '';
  closedAlert = false;

  @ViewChild('userFile', { static: false }) userFile: ElementRef;

  constructor(private fileService: FileService, public translate: TranslateService) {
    this.getHistorys(this.page);
  }

  ngOnInit() {
    this.getHistorys(this.page);
  }

  /**
   * Download Cause ALE Deviation file from datalake to local
   */
  onDownload() {
    this.isLoading = true;
    this.fileService.download(this.type, this.project)
      .subscribe(response => {
        let filename = null;
        switch (this.type) {
          case FileName.CAUSE_ALE:
            filename = 'cause_ale_deviation.csv';
            break;
          case FileName.CORRESP_ISO_CODEPAYS:
            filename = 'code_pays.csv';
            break;
          case FileName.CORRESPONDANCES:
            filename = 'table_correspondances.csv';
            break;
          case FileName.FACTOR_CAISSE:
            filename = 'load_factor_caisse.csv';
            break;
          case FileName.FACTOR_COMMERCE:
            filename = 'load_factor_commerce.csv';
            break;
          case FileName.PRIX_MOYEN:
            filename = 'table_prix_moyen.csv';
            break;
          case FileName.ROUTE_REF_COST:
            filename = 'route_ref_cost.csv';
            break;
          case FileName.TARIF_TRONCON:
            filename = 'tarif_troncon.csv';
            break;
          case FileName.TARIF_OP:
            filename = 'tarif_op.csv';
            break;
          case FileName.MODELE_EURO_KM:
            filename = 'modele_bpep.csv';
            break;
          case FileName.COMPTE_RATTACHEMENT:
            filename = 'compte_rattachement.csv';
            break;
          case FileName.BASE_COMPTES_FRANCE:
            filename = 'base_comptes_france.csv';
            break;
          default:
            break;
        }
        this.saveFile(response.body, filename);
        if (this.isKO) {
          this.isOK = true;
          this.typeAlert = 'danger';
          this.translate.get('notification.error_download').subscribe((text: string) => {
            this.notification = text;
          });

        } else {
          this.isOK = true;
          this.typeAlert = 'success';
          this.translate.get('notification.download').subscribe((text: string) => {
            this.notification = text;
          });
        }
        this.getHistorys(0);
        this.isLoading = false;
      });
  }

  /**
   * Save file on local
   * @param data
   * @param filename
   */
  saveFile(data: any, filename?: string) {
    const blob = new Blob([data], { type: 'text/csv; charset=utf-8' });
    if (blob.size === 0) {
      this.isKO = true;
    } else {
      fileSaver.saveAs(blob, filename);
    }

  }

  /**
   * Select file to upload
   * @param event
   */
  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  /**
   * clear input file
   */
  clearSelectedFile() {
    this.userFile.nativeElement.value = null;
  }

  /**
   * uplpoad file from local to the datalake
   */
  onUpload() {
    this.progress.percentage = 0;
    this.currentFileUpload = this.selectedFiles.item(0);
    this.fileService.upload(this.type, this.currentFileUpload).subscribe(event => {
      if (event.type === HttpEventType.UploadProgress) {
        this.progress.percentage = Math.round(100 * event.loaded / event.total);
        this.isLoading = true;
      } else if (event instanceof HttpResponse) {
        if (event && event.body) {
          const result = event.body[6] + '' + event.body[7] + '' + event.body[8] + '' + event.body[9];
          if (result === 'true') {
            this.isOK = true;
            this.typeAlert = 'success';
            this.translate.get('notification.upload').subscribe((text: string) => {
              this.notification = text;
            });
          } else {
            this.isOK = true;
            this.typeAlert = 'danger';
            this.translate.get('notification.error').subscribe((text: string) => {
              this.notification = text;
            });
          }
          this.getHistorys(0);
        } else {
          this.isOK = true;
          this.typeAlert = 'danger';
          this.translate.get('notification.error').subscribe((text: string) => {
            this.notification = text;
          });
        }
        this.isLoading = false;
      }

    });
    this.clearSelectedFile();
    this.selectedFiles = undefined;
  }


  /**
   * get all history
   *
   */
  getHistorys(page) {
    this.fileService.getHistorys(this.type, page).subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  /**
   * check response
   * @param response
   */
  private handleSuccessfulResponse(response) {
    if (response && response.historyDTOS && response.historyDTOS.content) {
      this.historys = response.historyDTOS.content;
      this.pageEmpty = response.historyDTOS.empty;
      this.totalPages = response.historyDTOS.totalPages;
      this.last = response.historyDTOS.last;
      this.first = response.historyDTOS.first;
      this.closedAlert = false;
      this.totalPagesArray = [];
      for (let i = 0; i < this.totalPages; i++) {
        this.totalPagesArray[i] = (i + 1);
      }
    }
  }

  /**
   * go to the onclick page number
   * @param page
   */
  goToPage(page: number) {
    this.page = page - 1;
    if (this.stringQuery && this.stringQuery.trim() !== '') {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }
  }

  /**
   * go to the previous page on the table
   */
  previousPage() {
    this.page = this.page - 1;
    if (this.stringQuery) {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }
  }

  /**
   * go to the next page on the table
   */
  nextPage() {
    this.page = this.page + 1;
    if (this.stringQuery) {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }
  }

  /**
   * set dslpr_type, this depends on your click
   * @param type
   */
  setType(type) {
    this.type = type;
    switch (this.type) {
      case FileName.CAUSE_ALE:
        this.classCauseAle = 'btn btn-default bg-warning col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Cause ALE Deviation"';
        break;
      case FileName.CORRESP_ISO_CODEPAYS:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-warning col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Code Pays"';
        break;
      case FileName.CORRESPONDANCES:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-warning col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Table de correspondances"';
        break;
      case FileName.FACTOR_CAISSE:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-warning col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Load factor caisse"';
        break;
      case FileName.FACTOR_COMMERCE:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-warning col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Load factor commerce"';
        break;
      case FileName.PRIX_MOYEN:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-warning col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Prix moyen"';
        break;
      case FileName.ROUTE_REF_COST:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-warning col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Route_ref_cost"';
        break;
      case FileName.TARIF_TRONCON:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-warning col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Tarif troncon"';
        break;
      case FileName.TARIF_OP:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-warning col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Tarif OP"';
        break;
      case FileName.MODELE_EURO_KM:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-warning col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Modele Euro KM"';
        break;
      case FileName.COMPTE_RATTACHEMENT:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-warning col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-dark col-lg-12';
        this.title = ' "Compte Rattachement"';
        break;
      case FileName.BASE_COMPTES_FRANCE:
        this.classCauseAle = 'btn btn-default bg-dark col-lg-12';
        this.classFactorCaisse = 'btn btn-default  bg-dark col-lg-12';
        this.classFactorCommerce = 'btn btn-default  bg-dark col-lg-12';
        this.classCorrespIsoCodePays = 'btn btn-default  bg-dark col-lg-12';
        this.classCorespondances = 'btn btn-default  bg-dark col-lg-12';
        this.classPrixMoyen = 'btn btn-default  bg-dark col-lg-12';
        this.classRouteRefCost = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifTroncon = 'btn btn-default  bg-dark col-lg-12';
        this.classTarifOP = 'btn btn-default  bg-dark col-lg-12';
        this.classModeleEuroKm = 'btn btn-default  bg-dark col-lg-12';
        this.classCompteRattachement = 'btn btn-default  bg-dark col-lg-12';
        this.classBaseComptesFrance = 'btn btn-default  bg-warning col-lg-12';
        this.title = ' "BASE COMPTES FRANCE"';
        break;
      default:
        break;
    }
    this.getHistorys(this.page);
  }

  /**
   * download error report
   * @param errorReport
   */
  onDownloadErrorReport(errorReport: string) {
    this.fileService.onDownloadErrorReport(errorReport)
      .subscribe(response => {
        const filename = errorReport;
        this.saveFile(response.body, filename);
        this.getHistorys(0);
      });
  }

  /**
   * search something in the table
   */
  searchHistorys() {
    if (this.stringQuery && this.stringQuery.trim() !== '') {
      this.fileService.searchHistorys(this.type, this.stringQuery.trim(), this.page).subscribe(
        response => this.handleSuccessfulResponse(response),
      );
    }
  }
}
