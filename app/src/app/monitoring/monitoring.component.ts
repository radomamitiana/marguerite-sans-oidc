import { Component, OnInit, TemplateRef, OnDestroy } from '@angular/core';
import * as fileSaver from 'file-saver';
import { FileService } from '../service/file.service';
import { HttpEventType, HttpResponse } from '@angular/common/http';
import { History } from '../data/History';
import { TranslateService } from '@ngx-translate/core';
import { FileName } from '../utils/FileName';
import { ProjectType } from '../utils/ProjectType';
import { MonitoringService } from '../service/monitoring/monitoring.service';
import { Monitoring } from '../data/Monitoring';

@Component({
  selector: 'app-monitoring',
  templateUrl: './monitoring.component.html',
  styleUrls: ['./monitoring.component.css']
})
export class MonitoringComponent implements OnInit, OnDestroy {

  selectedFiles: FileList;
  currentFileUpload: File;
  notification = '';
  isLoading = false;
  isOK = false;
  isKO = false;
  historys: History[];
  monitorings: Monitoring[];
  progress: { percentage: number } = { percentage: 0 };
  stringQuery: string;

  pageEmpty: true;
  totalPages: 1;
  totalPagesArray = [];
  last: false;
  first: false;
  page = 0;
  type = FileName.MONITORING;
  project = ProjectType.MONITORING;

  typeAlert = '';
  closedAlert = false;

  constructor(private fileService: FileService, public translate: TranslateService, private monitoringService: MonitoringService) {
    this.getMonitorings(this.page);
  }

  ngOnInit() {
    //this.onLoad();
    this.getMonitorings(this.page);
  }

  ngOnDestroy() {
    //this.deleteAll();

  }


  /**
   * Save file on local
   * @param data 
   * @param filename 
   */
  saveFile(data: any, filename?: string) {
    const blob = new Blob([data], { type: 'text/csv; charset=utf-8' });
    if (blob.size === 0) {
      this.isKO = true;
    } else {
      fileSaver.saveAs(blob, filename);
    }

  }

  /**
   * Select file to upload
   * @param event
   */
  selectFile(event) {
    this.selectedFiles = event.target.files;
  }

  /**
   * get all history
   * @param page 
   */
  getHistorys(page) {
    this.fileService.getHistorys(this.type, page).subscribe(
      response => this.handleSuccessfulResponse(response),
    );
  }

  getMonitorings(page) {
    this.monitoringService.getMonitorings(page).subscribe(
      response => this.handleSuccessfulResponse(response));
  }

  /**
   * check response
   * @param response 
   */
  private handleSuccessfulResponse(response) {
    console.log(response);
    if (response && response.monitoringDTOS && response.monitoringDTOS.content) {
      this.monitorings = response.monitoringDTOS.content;
      this.pageEmpty = response.monitoringDTOS.empty;
      this.totalPages = response.monitoringDTOS.totalPages;
      this.last = response.monitoringDTOS.last;
      this.first = response.monitoringDTOS.first;
      this.totalPagesArray = [];
      this.closedAlert = false;
      for (let i = 0; i < this.totalPages; i++) {
        this.totalPagesArray[i] = (i + 1);
      }
    }
  }

  /**
   * search something in the table
   */
  searchHistorys() {
    if (this.stringQuery && this.stringQuery.trim() !== '') {
      this.fileService.searchHistorys(this.type, this.stringQuery.trim(), this.page).subscribe(
        response => this.handleSuccessfulResponse(response),
      );
    }
  }


  /**
   * go to the onclick page number
   * @param page 
   */
  goToPage(page: number) {
    this.page = page - 1;
    /*if (this.stringQuery && this.stringQuery.trim() !== '') {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }*/

    this.getMonitorings(page);
  }

  /**
   * go to the previous page on the table
   */
  previousPage() {
    this.page = this.page - 1;
    /*if (this.stringQuery) {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }*/

    this.getMonitorings(this.page);
  }

  /**
   * go to the next page on the table
   */
  nextPage() {
    this.page = this.page + 1;
    /*if (this.stringQuery) {
      this.searchHistorys();
    } else {
      this.getHistorys(this.page);
    }*/

    this.getMonitorings(this.page);
  }

  /**
   * Load all data in the database
   */
  onLoad() {
    this.monitoringService.download(this.type).subscribe(response => {
      console.log(response.body);
      this.getMonitorings(this.page);
    });
  }

  /**
   * Delete All datain the database
   */
  deleteAll() {
    this.monitoringService.deleteAll(this.type).subscribe(response => {
      console.log("DELETE OK");
      this.getMonitorings(this.page);
    });
  }

}
