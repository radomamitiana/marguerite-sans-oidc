import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Account } from '../../data/Account';
import { Constant } from '../../utils/Constant';

const API_URL = Constant.get_Url();

@Injectable({
  providedIn: 'root'
})
export class AccountService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * get users's information
   */
  getAccount(ipn) {
    const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
    return this.httpClient.get<Account>(API_URL + '/api/user/' + sessionStorage.getItem('ipn') + '/roles', { headers });
  }
}
