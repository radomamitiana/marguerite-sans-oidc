import { HttpClient, HttpEvent, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { History } from '../data/History';
import { Constant } from '../utils/Constant';

const API_URL = Constant.get_Url();

@Injectable({
  providedIn: 'root'
})
export class FileService {

  constructor(private httpClient: HttpClient) {
  }

  /**
   * Download Cause ALE Deviation file from datalake to local
   */

  download(type: string, project: string): Observable<HttpResponse<String>> {
    return this.httpClient.get(API_URL + '/api/fileController/download?type=' + type + '&ipn=' + sessionStorage.getItem('ipn') + '&project=' + project, {
      observe: 'response',
      responseType: 'text'
    });
  }


  /**
   * Download Error report
   */

  onDownloadErrorReport(errorReport: string): Observable<HttpResponse<String>> {
    return this.httpClient.get(API_URL + '/api/fileController/downloadErrorReport/' + errorReport, {
      observe: 'response',
      responseType: 'text'
    });
  }

  /**
   * upload file from local to the datalake
   * @param type
   * @param file
   */
  upload(type: string, file: File): Observable<HttpEvent<{}>> {
    const formdata: FormData = new FormData();
    formdata.append('file', file);
    const req = new HttpRequest('POST', API_URL + '/api/fileController/upload?type=' + type + '&ipn=' + sessionStorage.getItem('ipn'), formdata, {
      reportProgress: true,
      responseType: 'text'
    });

    return this.httpClient.request(req);
  }

  /**
   * get all history
   * @param type
   * @param page
   */
  getHistorys(type: string, page) {
    return this.httpClient.get<History[]>(API_URL + '/api/history/list?page=' + page + '&type=' + type);
  }

  /**
   * search  history
   * @param type
   * @param stringQuery
   * @param page
   */
  searchHistorys(type: string, stringQuery, page) {
    return this.httpClient.get<History[]>(API_URL + '/api/history/list?stringQuery=' + stringQuery + '&page=' + page + '&type=' + type);
  }

}
