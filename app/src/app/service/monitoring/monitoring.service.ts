import { HttpClient, HttpEvent, HttpHeaders, HttpRequest, HttpResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Constant } from '../../utils/Constant';
import { Monitoring } from 'src/app/data/Monitoring';

const API_URL = Constant.get_Url();

@Injectable({
    providedIn: 'root'
})
export class MonitoringService {

    constructor(private httpClient: HttpClient) {
    }

    download(type: string): Observable<HttpResponse<String>> {
        return this.httpClient.get(API_URL + '/api/MitoringController/insertInto?type=' + type + '&ipn=' + sessionStorage.getItem('ipn'), {
            observe: 'response',
            responseType: 'text'
        });
    }

    /**
     * delete all records on table monitoring
     * @param type 
     */
    deleteAll(type: string) {
        const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
        return this.httpClient.delete(API_URL + '/api/MitoringController/deleteAll?type=' + type + '&ipn=' + sessionStorage.getItem('ipn'), { headers });
    }

    getMonitorings(page) {
        const headers = new HttpHeaders({ Authorization: 'Bearer ' + sessionStorage.getItem('accessToken') });
        return this.httpClient.get<Monitoring[]>(API_URL + '/api/MitoringController/list?page=' + page, { headers });
    }
}