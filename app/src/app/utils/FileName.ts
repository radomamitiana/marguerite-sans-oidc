export class FileName {
  //====================DSLPR File Name =====================//
  public static CAUSE_ALE = 'cause_ale';
  public static FACTOR_CAISSE = 'factor_caisse';
  public static FACTOR_COMMERCE = 'factor_commerce';
  public static CORRESP_ISO_CODEPAYS = 'corresp_iso_codepays';
  public static CORRESPONDANCES = 'correspondances';
  public static PRIX_MOYEN = 'prix_moyen';
  public static ROUTE_REF_COST = 'route_ref_cost';
  public static TARIF_TRONCON = 'tarif_troncon';
  public static TARIF_OP = 'tarif_op';
  public static MODELE_EURO_KM = 'modele_euro_km';
  public static COMPTE_RATTACHEMENT = 'compte_rattachement';
  public static BASE_COMPTES_FRANCE = 'base_comptes_france';


  //====================Monitoring File Name===============//
  public static MONITORING = 'monitoring';
}
