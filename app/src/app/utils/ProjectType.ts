export class ProjectType {
    public static DSLPR = 'dslpr';
    public static MONITORING = 'monitoring';
    public static CO2 = 'co2';
    public static TOP_ALLIANCE = 'top_alliance';
}